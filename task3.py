
def print_name():
    print("Marcin Basinski")


def num_sum(a: float, b: float, c: float):
    print(a + b + c)


def get_hello_world() -> str:
    return "Hello World"


def get_num_difference(a: float, b: float) -> float:
    return abs(a - b)


class Cake:
    pass


def get_cake() -> Cake:
    return Cake()


print(isinstance(get_cake(), Cake))
